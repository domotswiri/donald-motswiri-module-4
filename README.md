# donald-motswiri-module-4

M4-Assessment 2

## Instructions:
Create a Gitlab account
Create a repository on Gitlab as name-surname-module-4

### Assessment:
All the code must be submitted through a Gitlab repository link. 

1. Add an animation splash screen.
1. Create a common theme for your app, from previous class.
1. Implement design onto your app.