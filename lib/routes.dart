// ignore_for_file: prefer_const_constructors

import 'package:e_commerce_ui/screens/cart/cart_screen.dart';
import 'package:e_commerce_ui/screens/complete_profile/complete_profile_screen.dart';
import 'package:e_commerce_ui/screens/forgot_password/forgot_password_screen.dart';
import 'package:e_commerce_ui/screens/home/home_screen.dart';
import 'package:e_commerce_ui/screens/otp/otp_screen.dart';
import 'package:e_commerce_ui/screens/product_details/product_detais_screen.dart';
import 'package:e_commerce_ui/screens/profile/profile_screen.dart';
import 'package:e_commerce_ui/screens/sign_in/sign_in_screen.dart';
import 'package:e_commerce_ui/screens/sign_up/sign_up_screen.dart';
import 'package:e_commerce_ui/screens/splash/loading_screen.dart';
import 'package:e_commerce_ui/screens/splash/splash_screen.dart';
import 'package:e_commerce_ui/screens/success/success_screen.dart';
import 'package:flutter/widgets.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  LoadingScreen.routeName: (context) => LoadingScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SuccessScreen.routeName: (context) => SuccessScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfile.routeName: (context) => CompleteProfile(),
  OTPScreen.routeName: (context) => OTPScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  ProductDetailsScreen.routeName: (context) => ProductDetailsScreen(),
  CartScreen.routeName: (context) => CartScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen()
};
