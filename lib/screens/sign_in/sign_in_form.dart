import 'package:e_commerce_ui/components/form_error.dart';
import 'package:e_commerce_ui/screens/cart/cart_screen.dart';
import 'package:e_commerce_ui/screens/profile/profile_screen.dart';
import 'package:e_commerce_ui/screens/success/success_screen.dart';
import 'package:flutter/material.dart';

import '../../components/default_button.dart';
import '../../components/suffix_icon.dart';
import '../../constants.dart';
import '../../size_config.dart';
import '../forgot_password/forgot_password_screen.dart';

class SignForm extends StatefulWidget {
  const SignForm({
    Key? key,
  }) : super(key: key);

  @override
  State<SignForm> createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  String email = '';
  String password = '';
  bool rememberme = false;

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error!);
      });
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(16)),
          buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(16)),
          Row(
            children: [
              Checkbox(
                value: rememberme,
                activeColor: kPrimaryColor,
                onChanged: (value) => setState(() => rememberme = value!),
              ),
              GestureDetector(
                onTap: () => setState((() => rememberme = !rememberme)),
                child: const Text('Remember me'),
              ),
              const Spacer(),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                      (context), ForgotPasswordScreen.routeName);
                },
                child: const Text(
                  'Forgot Password',
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(16)),
          DefaultButton(
            text: 'Login',
            press: () {
              // if (_formKey.currentState!.validate()) {
              //   _formKey.currentState!.save();
              //   Navigator.pushNamed(context, SuccessScreen.routeName);
              // }

              // Skip validation
              Navigator.pushNamed(context, ProfileScreen.routeName);
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        hintText: 'Enter your password',
        labelText: 'Password',
        suffixIcon: SuffixIcon(svgIcon: 'assets/icons/Lock.svg'),
      ),
      onSaved: (newValue) => password = newValue!,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        hintText: "Enter your e-mail",
        labelText: 'Email',
        suffixIcon: SuffixIcon(svgIcon: 'assets/icons/Mail.svg'),
      ),
      onSaved: (newValue) => email = newValue!,
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
      },
    );
  }
}
