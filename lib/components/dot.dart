import 'package:flutter/material.dart';

import '../constants.dart';

class Dot extends StatelessWidget {
  const Dot({
    Key? key,
    required this.index,
    required this.currentPage,
  }) : super(key: key);

  final int index;
  final int currentPage;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      height: 6,
      width: currentPage == index ? 20 : 6,
      margin: const EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : const Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}
