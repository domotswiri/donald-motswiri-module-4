import 'package:flutter/material.dart';

import '../size_config.dart';

class SectionTitle extends StatelessWidget {
  final VoidCallback press;
  final String text;
  final String buttonText;

  const SectionTitle({
    Key? key,
    required this.press,
    required this.text,
    this.buttonText = 'See More',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text,
            style: TextStyle(
              fontSize: getProportionateScreenWidth(18),
              color: Colors.black,
            ),
          ),
          GestureDetector(onTap: press, child: Text(buttonText))
        ],
      ),
    );
  }
}
